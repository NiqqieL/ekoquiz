import React, {useRef} from 'react';
import FlipNumbers from 'react-flip-numbers'
import './App.css';


const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop)

class App extends React.Component
{
  constructor(props)
  {
    super(props)
    // const myRef = useRef(null)
    this.myDiv = React.createRef()

    // this.p1_link = '/index';
  }

  state = {
    timeRemaining: 0,
  };

  componentDidMount() {
    document.title = 'EkoQuiz';
    this.timer = setInterval(() => {
      this.setState({
        timeRemaining: this.state.timeRemaining + 25,
      });
    }, 100);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render()
  {

    return (

      <div className='all'>

              <div className='counter'>
                <div className='clockWrapper'>
                <FlipNumbers play color="#757C21" perspective={500} background="#00000" width={60} height={60} numbers={`${this.state.timeRemaining}`}/>
                kg śmieci!
                </div>
                <div className='clockDescription'>
                Tyle zostało wyrzucone od momentu kiedy wszedłeś na tę stronę! Co możemy z tym zrobic?
                </div>
              </div>


              <div className='counterArticleBackground'>
                  <div className='couterArticle'>
                    <center>
                      Na całym świecie w ciągu roku ponad 8 milionów ton plastiku ląduje w morzach i oceanach. 
                      To tak, jakby wyrzucać do nich co minutę zawartość jednej śmieciarki. Większość z tych odpadów możemy bez problemu zastąpić innymi - niezagrażającymi środowisku.
                      <br/>Tylko w Polsce zużywamy rocznie 1,2 miliarda słomek, a w całej Unii Europejskiej aż 100 miliardów foliówek i 46 miliardów plastikowych butelek rocznie.  
                      Ponad milion zwierząt ginie rocznie przez plastikowe śmieci w morzach i oceanach. Produkty nazywane <br/>jednorazowymi, z perspektywy ludzkiego życia są wieczne – tworzywa sztuczne rozkładają się setki a nawet tysiące lat.
                      <a target="_blank"
                      className='articleLink'
                      rel="noopener noreferrer"
                      href="https://www.wwf.pl/aktualnosci/wakacje-w-morzu-plastiku-i-oceanie-toksyn?fbclid=IwAR1ZBCsZM7w7VepLTz4dX2gPBsrGY5uBVQEcO0_Q9k4_qG9_irvCQ3qCgEo">
                      &nbsp; Dowiedz się więcej...
                     </a>
                    </center>  
                  </div> 
              </div>

        <div className='buttonsHeading'> Co możesz zrobić?</div>

        <div className='buttonsBackground'> 

          <div className='buttons'>

           <a className='quizButton' href="https://quiz310300.github.io/?fbclid=IwAR3WXvTifEIrrpBeBud_sBv1fgULMsLiH3g9SAXyEAIGMyRS_MrFFXL27r4" >Quiz </a>

            <div className='articleButton' onClick={() => { this.myDiv.current.scrollIntoView({behavior:'smooth', block:'start'})}}>  Dowiedz się więcej </div>

          </div>

        </div>

        <div className='articleHeading' ref={this.myDiv}> Artykuły </div>      

        <div  className='articleBackground' >
        <center>

        <br/>

              <div className='article'>
                
                <div className='articleTitle' onClick> Sposoby na oszczędzanie wody, czyli jak być eko we własnym domu </div>

                <div className='articleContent'> 
                  Czy wiesz o tym, że zaledwie 2,5% światowych zasobów wody to woda słodka, a mniej niż 0,01% stanowi woda pitna odnawialna dzięki odpadom? 
                  <br/>W związku z wyraźnym deficytem wody na świecie bycie eko to nie moda czy chwilowy trend, ale konieczność. Wbrew pozorom taka postawa wcale nie musi się wiązać z radykalną zmianą swojego stylu życia.
                  Proste zasady można naturalnie wprowadzić do swojej codzienności. Zmniejszenie zużycia wody pozwoli nie tylko zaoszczędzić cenne zasoby, ale też zmniejszyć rachunki za media. 
                  Przekonaj się, że oszczędzanie wody może być prostsze niż myślisz. 
                 <a target="_blank"
                   className='articleLink'
                   rel="noopener noreferrer"
                   href="https://www.techsterowniki.pl/blog/segregacja-smieci-i-oszczedzanie-wody---czyli-jak-byc-eko-we-wlasnym-domu?fbclid=IwAR1L2LbX3h84A3htMDLtMmXmsrlW5ChayiZeAhwswk6HWUw7H14iuZ5eirk">
                  &nbsp; Dowiedz się więcej...
                </a>
                 </div>

              </div>

        <br/>

              <div className='article' >

                <div className='articleTitle'> Wyspa śmieci ma Pacyfiku 5 razy większa od Polski."Niedługo w oceanach będzie pływać więcej plastiku niż ryb" </div>

                <div className='articleContent'>
                   Wielka Pacyficzna Plama Śmieci powiększa się dużo szybciej niż przewidywali naukowcy. 
                  Z badań fundacji The Ocean Cleanup wynika, że w tej chwili zajmuje ona powierzchnię ok. 1,6 miliona km kw., czyli pięć razy tyle co powierzchnia naszego kraju .
                  Olbrzymia plama śmieci, zlokalizowana w północnej części Pacyfiku na północny wschód od Hawajów, to jeden z najdobitniejszych dowodów niszczycielskiej działalności człowieka . 
                  Obserwując to miejsce specjaliści stwierdzili, że do połowy XXI w. w oceanach będzie pływać więcej plastiku niż ryb. 
                <a target="_blank"
                   className='articleLink'
                   rel="noopener noreferrer"
                   href="https://www.wp.pl/?s=turystyka.wp.pl%2Fsgwpsgfirst-6241784715253377a&fbclid=IwAR30AWvyeZKxdYqQ3o3AvHpnNBtK7IN76HJF4zk9EoQvR46bHHQ0l8yXTrc&wga_sa=section-sgfirst&wga_srt=none&wga_isSgFirst=true">
                   &nbsp; Dowiedz się więcej...
                </a>
                </div>
              </div>  
                

        <br/>      

              <div className='article'>    
                <div className='articleTitle'>  W ten sposób marnujesz najwięcej wody. 10 złych nawyków </div>

                <div className='articleContent'>
                Nie zakręcasz kranu podczas szczotkowania zębów? Zanim nalejesz wody do szklanki czekasz aż „zejdzie” ciepła? Niedobrze, a jest tego znacznie więcej.
                Oszczędzanie wody to nie tylko sposób na niższe rachunki, to przede wszystkim pomoc dla naszej planety, która naprawdę ma już dość z nami problemów. 
                <a target="_blank"
                   className='articleLink'
                   rel="noopener noreferrer"
                   href="https://www.national-geographic.pl/ludzie/w-ten-sposob-marnujesz-najwiecej-wody-10-zlych-nawykow?fbclid=IwAR0FADYrYMSHknFaPI3xBA6MREz6CvGY_VHoCb0Num83YZlO-iReE4kf4mY">
                   &nbsp; Dowiedz się więcej...
                </a>
                </div>
              </div>    

        <br/>       

              <div className='article'>
                <div className='articleTitle'>10 zasad oszczędzania energii</div>
                <div className='articleContent'>
                Podczas procesu produkcji energii, mamy do czynienia z pewnymi tego skutkami ubocznymi. 
                Jest to głównie emisja szkodliwych substancji np. dwutlenku węgla. Następstwem tego jest efekt cieplarniany, kwaśne deszcze i dziura ozonowa. 
                Co powinniśmy robić, aby nie marnować niepotrzebnie energii, a tym samy chronić środowisko i zaoszczędzić na domowych rachunkach?
                <a target="_blank"
                   className='articleLink'
                   rel="noopener noreferrer"
                   href="https://deccoria.pl/artykuly/artykuly/10-zasad-oszczedzania-energii-5-1411?fbclid=IwAR3gp22JMK4tPH1Uef71BF-bAnG2dsiurb0lQ6C6S2qBK6NUYYcpgJ9yY0M">
                   &nbsp; Dowiedz się więcej...
                </a>
                </div>
              </div>  

        <br/>    

              <div className='article'>
                <div className='articleTitle'>Wszystko co powinieneś wiedzieć o zanieczyszczeniu powietrza</div>
                <div className='articleContent'>
                Czym jest więc zanieczyszczenie powietrza?
                  Smog jest nienaturalnym zjawiskiem atmosferycznym, które polega na współistnieniu związków chemicznych oraz pyłów w naszej atmosferze. 
                  Przebywanie, oddychanie nim, zagraża naszemu zdrowiu i życiu. Pochodzenie słowa smog ma swoje korzenie w dwóch anglojęzycznych słowach: smoke – dym oraz fog – mgła.
                  <a target="_blank"
                  className='articleLink'
                   rel="noopener noreferrer"
                   href="https://airly.eu/pl/wszystko-co-powinienes-wiedziec-o-zanieczyszczeniu-powietrza">
                   &nbsp; Dowiedz się więcej...
                </a>
                </div>
              </div>

        <br/>  

        </center>
        </div>

        <div className='footer'> 2019 </div>

      </div>
    );
  }
}
export default App;
